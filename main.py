import tensorflow as tf
import numpy as np
import logging
import matplotlib.pyplot as plt

import os

logger = tf.get_logger()
logger.setLevel(logging.ERROR)

class VerilogPerceptronGenerator:

    def __init__(self):
        self.module_name_verilog_line = "module neuron("
        self.input_verilog_first_part = "input ["
        self.weight_verilog_last_part = "] weight,"
        self.bias_verilog_last_part = "] bias,"
        self.output_verilog_first_part = "output reg ["
        self.module_initial_line = "  initial"
        self.module_begin_line = "    begin"
        self.module_main_func_left_side_first_part = "      assign {"
        self.module_main_func_left_side_middle_part = "} "
        self.module_main_func_right_side_first_part = "("
        self.module_main_func_right_side_middle_part = " * weight)"
        self.module_main_func_right_side_bias_part = " + bias"
        self.module_main_func_right_side_last_part = ";"
        self.module_end_line = "    end"
        self.module_last_line = "endmodule"

        self.verilog_code = list()
        return

    def generateNeuron(self, inputMin, inputMax, inputName, weightMin, weightMax, isBiasUsed, biasMin, biasMax, outputMin, outputMax, outputName):
        self.verilog_code.clear()
        self.verilog_code.append(self.module_name_verilog_line + "\n")
        input_text = "  " + self.input_verilog_first_part + str(inputMax)
        input_text+= ":" + str(inputMin) + "] " + inputName + ","
        self.verilog_code.append(input_text + "\n")
        input_text = "  " + self.input_verilog_first_part + str(weightMax)
        input_text+= ":" + str(weightMin) + self.weight_verilog_last_part
        self.verilog_code.append(input_text + "\n")
        if(isBiasUsed):
            input_text = "  " + self.input_verilog_first_part + str(biasMax)
            input_text+= ":" + str(biasMin) + self.bias_verilog_last_part
            self.verilog_code.append(input_text + "\n")

        input_text = "  " + self.output_verilog_first_part + str(outputMax)
        input_text+= ":" + str(outputMin) + "] " + outputName + ")"
        self.verilog_code.append(input_text + "\n")

        self.verilog_code.append(self.module_initial_line + "\n")
        self.verilog_code.append(self.module_begin_line + "\n")

        input_text = self.module_main_func_left_side_first_part + outputName + self.module_main_func_left_side_middle_part + " = "
        input_text+= self.module_main_func_right_side_first_part + inputName + self.module_main_func_right_side_middle_part
        if(isBiasUsed): input_text+= self.module_main_func_right_side_bias_part
        input_text+= self.module_main_func_right_side_last_part
        self.verilog_code.append(input_text + "\n")

        self.verilog_code.append(self.module_end_line + "\n")
        self.verilog_code.append(self.module_last_line + "\n")
        return

    def exportNeuron(self, filename):

        f = open(filename, "a")
        
        for line in self.verilog_code:
            f.writelines(line)

        f.close()

        return

celsius_q    = np.array([-40, -35, -30, -25, -20, -15, -10, -5,  0,    7, 15,   22,   29,    38,    46,    52],  dtype=float)
fahrenheit_a = np.array([-40, -31, -22, -13,  -4,   5,  14, 23, 32, 44.6, 59, 71.6, 84.2, 100.4, 114.8, 125.6],  dtype=float)

for i,c in enumerate(celsius_q):
    print("{} degrees Celsius = {} degrees Fahrenheit".format(c, fahrenheit_a[i]))

l0 = tf.keras.layers.Dense(units=1, input_shape=[1])

model = tf.keras.Sequential([l0])

model.compile(loss='mean_squared_error', optimizer=tf.keras.optimizers.Adam(0.1))

history = model.fit(celsius_q,fahrenheit_a,epochs=500,verbose=False)
print("Finished Training the Model")

plt.xlabel('Epoch Number')
plt.ylabel('Loss Magnitude')
plt.plot(history.history['loss'])

plt.show()

print(model.predict([8]))

print("These are the layer variables: {}".format(l0.get_weights()))

print("Now saving the model to HDD")

model.save(filepath="tf_model.tf", overwrite=True, save_format='tf')

print("Now loading the saved model from HDD")

new_model = tf.keras.models.load_model('tf_model.tf')

print(model.predict([10]))

weights = new_model.layers[0].get_weights()

print("These are the layer variables: {}".format(weights))

perceptronGenerator = VerilogPerceptronGenerator()

perceptronGenerator.generateNeuron(0, 31, "celsius", 0, 31, True, 0, 31, 0, 31, "fahrenheit")

perceptronGenerator.exportNeuron("NeuronCode.verilog")